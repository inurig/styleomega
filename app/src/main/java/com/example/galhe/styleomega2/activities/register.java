package com.example.galhe.styleomega2.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.models.User;

public class register extends AppCompatActivity {
    private EditText firstName ;
    private EditText email;
    private EditText Password;
    private  EditText confirm ;
    private EditText address;
    private String fname;
    private String uname;
    private String password;
    private String cpassword;
    private String Address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }


    public void register(View view){

        initialize();

        if(!validate()){
            Toast.makeText(this,"Sign Up Has Failed",Toast.LENGTH_SHORT).show();
        }else {
            if (cpassword.equals(password)) {
                User users = new User(uname, password, fname,Address);
                users.save();

                Intent intent = new Intent(register.this, login.class);
                startActivity(intent);

                Toast.makeText(getApplicationContext(), "Successfully Registered: " + fname, Toast.LENGTH_LONG).show();

            }else{
                confirm.setError("mismatched password!");
            }
        }

    }


    public void initialize(){
        firstName = (EditText) findViewById(R.id.reg_fname);
        email = (EditText) findViewById(R.id.reg_uname);
        Password = (EditText) findViewById(R.id.reg_pass);
        confirm = (EditText) findViewById(R.id.reg_confirm);
        address = (EditText) findViewById(R.id.reg_address);

        fname = firstName.getText().toString().trim();
        uname = email.getText().toString().trim();
        password = Password.getText().toString().trim();
        cpassword = confirm.getText().toString().trim();
        Address = address.getText().toString().trim();

    }


    public boolean validate(){
        boolean valid=true;

        if(fname.isEmpty()){
            firstName.setError("Enter First Name");
            valid=false;
        }
        if(uname.isEmpty()){

            email.setError("Enter Email");
            valid=false;
        }

        if (!uname.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            email.setError("Invalid Email Address");
            valid=false;
        }
        if(Address.isEmpty()){

            address.setError("Enter You'r Address");
            valid=false;
        }
        if(password.isEmpty()){

            Password.setError("Enter a password");
            valid=false;
        }
        if(cpassword.isEmpty()){
            confirm.setError("Please confirm password");
            valid=false;
        }

        return valid;
    }

}
