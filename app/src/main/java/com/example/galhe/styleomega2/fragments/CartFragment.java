package com.example.galhe.styleomega2.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.activities.cartAdapter;
import com.example.galhe.styleomega2.models.OrderedProducts;
import com.example.galhe.styleomega2.models.Product;
import com.example.galhe.styleomega2.models.User;
import com.example.galhe.styleomega2.models.cart;

import java.util.Iterator;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by galhe on 5/30/2018.
 */

public class CartFragment extends Fragment {
    Button removeAllProducts;
    Button PurchaseProduct;
    double total=0;

    List<cart> bookedItem;
    ListView productL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //try {
        View view = inflater.inflate(R.layout.cart_fragment_layout,container,false);
        SharedPreferences preferences = getActivity().getSharedPreferences("UserDetails", Context.MODE_PRIVATE);

        final String id = preferences.getString("Uid","" );

        bookedItem = cart.findWithQuery(cart.class, "Select * from cart where status = ?", "Pending");



         productL = view.findViewById(R.id.pl);


           final OrderedProducts op = new OrderedProducts();
           final User user = User.findById(User.class,Long.parseLong(id));



        final  Button checkout = view.findViewById(R.id.checkoutb);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                 ft.replace(R.id.fragment_container , new Checkout_fragment());
                ft.addToBackStack(null);
                 ft.commit();


            }
        });

            cartAdapter adapter = new cartAdapter(getActivity(), bookedItem);
            productL.setAdapter(adapter);

          for (cart c:bookedItem){
              total=total+c.getP().getPrice();

          }
      EditText  TTotal = (EditText)view.findViewById(R.id.total);
        TTotal.setText(String.valueOf(total));
          return view;
        }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("My Cart");
    }


}

