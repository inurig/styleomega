package com.example.galhe.styleomega2.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.models.User;
import com.orm.SugarRecord;

import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by galhe on 5/1/2018.
 */

public class f_profile extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);


        final TextView fname = (TextView) view.findViewById(R.id.pro_firstN);
        final TextView email = (TextView) view.findViewById(R.id.proEmail);

        final TextView address = (TextView) view.findViewById(R.id.upAddress);

        final TextView oldpwrd = (TextView) view.findViewById(R.id.oldpassword);
        final TextView newpwrd = (TextView) view.findViewById(R.id.newpword);
        final TextView confirmpwrd = (TextView) view.findViewById(R.id.confirmpword);
        Button updatepro = (Button) view.findViewById(R.id.updatepro);
        Button updatepass = (Button) view.findViewById(R.id.Updatepword);

        updatepro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    updatedetail(fname.getText().toString(),email.getText().toString(),address.getText().toString());

            }
        });

        updatepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatepwrd(oldpwrd.getText().toString(), newpwrd.getText().toString(), confirmpwrd.getText().toString());
            }
        });
        return view;
    }


    public void updatedetail(String fname,String email, String address) {
        SharedPreferences preferences = getActivity().getSharedPreferences("UserDetails", MODE_PRIVATE);
        String id = preferences.getString("Uid", "");

        List<User> us = User.listAll(User.class);
        User u = SugarRecord.findById(User.class, Long.parseLong(id));

        u.setFirstName(fname);
        u.setEmail(email);
        u.setAddress(address);
        u.save();
        List<User> un = User.listAll(User.class);
        Toast.makeText(getActivity(), "Profile Successfully updated", Toast.LENGTH_LONG).show();
    }

    public void updatepwrd(String password, String newpassword, String confirmpassword) {

        if (newpassword.equals(confirmpassword)) {
            SharedPreferences preferences = getActivity().getSharedPreferences("UserDetails", MODE_PRIVATE);
            String id = preferences.getString("Uid", "");
            List<User> us = User.listAll(User.class);
            User u = SugarRecord.findById(User.class, Long.parseLong(id));

         u.setPassword(newpassword);
            u.save();
            List<User> uk = User.listAll(User.class);
            Toast.makeText(getActivity(), "Password Successfully updated", Toast.LENGTH_LONG).show();
        }

    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("My profile");
    }
}
