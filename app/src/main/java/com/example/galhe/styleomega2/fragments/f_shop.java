package com.example.galhe.styleomega2.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.activities.ImageAdaptor;
import com.example.galhe.styleomega2.models.Product;

import java.util.List;

/**
 * Created by galhe on 5/1/2018.
 */

public class f_shop extends android.support.v4.app.Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.wallet_fragment, container, false);
        Button female = (Button)view.findViewById(R.id.female);

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           String id="Female";
                f_home fragment = new f_home();
                Bundle bundle = new Bundle();
                bundle.putString("ID", id);
                fragment.setArguments(bundle);
                android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        Button male = (Button)view.findViewById(R.id.male);

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id="Male";
                f_home fragment = new f_home();
                Bundle bundle = new Bundle();
                bundle.putString("ID", id);
                fragment.setArguments(bundle);
                android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        Button kids = (Button)view.findViewById(R.id.kids);

        kids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id="Kids";
                f_home fragment = new f_home();
                Bundle bundle = new Bundle();
                bundle.putString("ID", id);
                fragment.setArguments(bundle);
                android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        Button acc = (Button)view.findViewById(R.id.acc);

        acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id="Accessory";
                f_home fragment = new f_home();
                Bundle bundle = new Bundle();
                bundle.putString("ID", id);
                fragment.setArguments(bundle);
                android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Style Omega");
    }
}
