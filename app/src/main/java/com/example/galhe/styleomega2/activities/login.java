package com.example.galhe.styleomega2.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.models.User;

import java.util.List;

public class login extends AppCompatActivity {
    private EditText email;
    private EditText Password;
    private Button Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
    public void registerMethod(View view){
        Intent intent = new Intent(login.this, register.class);
        startActivity(intent);
    }

    public void loginMethod(View view){
        email = (EditText) findViewById(R.id.uname);
        Password =  (EditText) findViewById(R.id.oldpassword);
        Login = (Button) findViewById(R.id.blogin);


        validate(email.getText().toString(),Password.getText().toString());
    }


    public void validate(final String email, final String Password) {
        List<User> users = User.find(User.class, "email=? and password=?", email, Password);
        for (User user : users) {
            if (users.size() == 1) {

                SharedPreferences pref = getSharedPreferences("UserDetails" , MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("Uid",user.getId().toString());
                edit.apply();

                Intent intent = new Intent(login.this, navi.class);

                startActivity(intent);
                this.finish();

            } else if (email.isEmpty() && Password.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Please enter email and password! ", Toast.LENGTH_LONG).show();
                //Toast.makeText(this, "Please enter email and password!", Toast.LENGTH_SHORT).show();
            } else if (email.isEmpty()) {
                Toast.makeText(this, "Please enter email!", Toast.LENGTH_SHORT).show();
            } else if (Password.isEmpty()) {
                Toast.makeText(this, "Please enter Password!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Email or password not valid!", Toast.LENGTH_SHORT).show();
            }


        }
    }
}
