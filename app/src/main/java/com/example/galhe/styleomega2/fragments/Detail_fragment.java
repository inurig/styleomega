package com.example.galhe.styleomega2.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.models.Product;
import com.example.galhe.styleomega2.models.User;
import com.example.galhe.styleomega2.models.cart;
import com.squareup.picasso.Picasso;

public class Detail_fragment extends android.support.v4.app.Fragment {
ImageButton shareButton;
Button addtocart;
cart Cart;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences pref = getActivity().getSharedPreferences("UserDetails", Context.MODE_PRIVATE);
        String id = pref.getString("Uid", "");

        LayoutInflater i = LayoutInflater.from(getContext());
        View view = i.inflate(R.layout.detail_fragment, container, false);

        android.support.v4.app.Fragment fragment = new Detail_fragment();
        Bundle bundle = getArguments();

        Long r = null;
        String z = null;
        try {
            r = bundle.getLong("id");
            String q = bundle.getString("Name");
            Double y = bundle.getDouble("Price");
            z = bundle.getString("Image");
            String a = bundle.getString("Description");
            Double x = bundle.getDouble("Quantity");




        TextView pprice = (TextView) view.findViewById(R.id.ProductPrice);
        TextView pname = (TextView) view.findViewById(R.id.ProductName);
        TextView pdetail = (TextView) view.findViewById(R.id.productDetails);
        TextView pquantity = (TextView) view.findViewById(R.id.ProductQuantity);

        ImageView img = (ImageView) view.findViewById(R.id.productImage);

        final Product product = Product.findById(Product.class, r);
        final User user = User.findById(User.class,Long.parseLong(id));


            pprice.setText(y.toString());
           pname.setText(q);
           pdetail.setText(a);
           pquantity.setText(x.toString());







            Picasso.get()
                    .load(z)
                    .placeholder(R.drawable.placeholder)
                    .into(img);


        shareButton = view.findViewById(R.id.shareButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_SUBJECT,"My New App");

                share.putExtra(Intent.EXTRA_SUBJECT,"Try My New App");
                startActivity(Intent.createChooser(share,"Share Via"));
            }
        });
        addtocart = (Button)view.findViewById(R.id.AddtocartB);
        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Cart = new cart();
                    Cart.setProduct(product);
                    Cart.setUser(user);
                    Cart.setStatus("Pending");
                    Cart.save();
                    Toast.makeText(getActivity(), "Product Successfully Added to cart", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Product Details");
    }

}
