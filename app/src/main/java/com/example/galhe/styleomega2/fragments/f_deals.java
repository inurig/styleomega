package com.example.galhe.styleomega2.fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.activities.PHAdapter;
import com.example.galhe.styleomega2.models.OrderedProducts;
import com.example.galhe.styleomega2.models.Product;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by galhe on 5/1/2018.
 */

public class f_deals extends android.support.v4.app.Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_fragment,container,false);
        SharedPreferences preferences = getActivity().getSharedPreferences("UserDetails" , MODE_PRIVATE);

        final String id = preferences.getString("Uid","");
        final List<Product>plist = Product.listAll(Product.class);
        ListView lv = view.findViewById(R.id.historyList);

        List<OrderedProducts> op = OrderedProducts.listAll(OrderedProducts.class);
        PHAdapter adapter = new PHAdapter(getActivity(),op);
        lv.setAdapter(adapter);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Purchase History");
    }
}
