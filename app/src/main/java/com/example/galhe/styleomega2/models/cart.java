package com.example.galhe.styleomega2.models;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by galhe on 5/30/2018.
 */

public class cart extends SugarRecord  {
    @SerializedName("product")
    private Product product;
    @SerializedName("user")
    private  User user;
    @SerializedName("status")
    private  String status;

    public Product getP() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public cart(){

    }

    public cart (Product product, User user,String status){
        this.product= product;
        this.user= user;
        this.status=status;


    }
}

