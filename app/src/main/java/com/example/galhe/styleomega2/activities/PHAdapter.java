package com.example.galhe.styleomega2.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.models.OrderedProducts;
import com.example.galhe.styleomega2.models.cart;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by galhe on 6/1/2018.
 */

public class PHAdapter extends ArrayAdapter<OrderedProducts>  {

   OrderedProducts op;
    List<OrderedProducts> object;


    public PHAdapter(Context context, List<OrderedProducts> objects) {
        super(context, R.layout.custom_cart_layout, objects);
        this.object=objects;
    }


    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inf = LayoutInflater.from(getContext());
        View  customView = inf.inflate(R.layout.custom_history_layout,parent, false);
        op = getItem(position);


       Button b = customView.findViewById(R.id.cancelB);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(op);
                op.delete();

            }
        });



    TextView Text1 = customView.findViewById(R.id.name);
    TextView Text2 = customView.findViewById(R.id.price);
    ImageView Img = customView.findViewById(R.id.historyimage);
    Text1.setText(op.getC().getP().getName());
     Text2.setText(String.valueOf(op.getC().getP().getPrice()));
           Picasso.get()
                .load(op.getC().getP().getScaledImage())
                .placeholder(R.drawable.ic_launcher_background)
                .resize(300, 400)
                .into(Img);


        return customView;
    }


}
