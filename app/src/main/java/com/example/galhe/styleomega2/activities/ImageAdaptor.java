package com.example.galhe.styleomega2.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.galhe.styleomega2.models.Product;
import com.squareup.picasso.Picasso;

import com.example.galhe.styleomega2.R;

import java.util.List;

/**
 * Created by galhe on 5/3/2018.
 */

public class ImageAdaptor extends ArrayAdapter<Product> {


    private  Context Context;
    private LayoutInflater inflater;


    public ImageAdaptor(Context context, List<Product> products) {
      super(context,R.layout.gridview_layout,products);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         Product product = getItem(position);
         LayoutInflater Infl = LayoutInflater.from(getContext());
        convertView = Infl.inflate(R.layout.gridview_layout, null);


         ImageView imageView = (ImageView)convertView.findViewById(R.id.imageview_cover_art);
         TextView nameTextView = (TextView)convertView.findViewById(R.id.textview_product_name);

        Picasso.get()
                .load(product.getScaledImage()) //Load the image
                //.fit()
              //  .load("https://www.androidtutorialpoint.com/wp-content/uploads/2016/10/interstellar.jpg")
                .placeholder(R.drawable.placeholder) //Image resource that act as placeholder
                .error(R.drawable.placeholder) //Image resource for error
                .resize(300, 500)  // Post processing - Resizing the image
                .into(imageView); // View where image is loaded.

        nameTextView.setText(product.getName());

        return convertView;
    }
}
