package com.example.galhe.styleomega2.models;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Product extends SugarRecord {

    @SerializedName("Name")
    private String Name;
    @SerializedName("ShortDescription")
    private String ShortDescription;
    @SerializedName("LongDescription")
    private String LongDescription;
    @SerializedName("Price")
    private double Price;
    @SerializedName("Quantity")
    private double Quantity;
    @SerializedName("Active")
    private boolean Active;
    @SerializedName("ScaledImage")

    private String ScaledImage;
    @SerializedName("FullImage")
    private String FullImage;
    @SerializedName("Category")
    private String Category;



    public Product( String name, String shortDescription, String longDescription,  double price, double quantity, boolean active, String scaledImage, String fullImage,String Category) {

        Name = name;
        ShortDescription = shortDescription;
        LongDescription = longDescription;

        Price = price;
        Quantity = quantity;
        Active = active;
        ScaledImage = scaledImage;
        FullImage = fullImage;
        Category = Category;
    }

    public Product() {
    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public String getLongDescription() {
        return LongDescription;
    }

    public void setLongDescription(String longDescription) {
        LongDescription = longDescription;
    }



    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getScaledImage() {
        return ScaledImage;
    }

    public void setScaledImage(String scaledImage) {
        ScaledImage = scaledImage;
    }

    public String getFullImage() {
        return FullImage;
    }



    public void setFullImage(String fullImage) {
        FullImage = fullImage;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }
}



