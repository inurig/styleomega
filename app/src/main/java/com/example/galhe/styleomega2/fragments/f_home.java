package com.example.galhe.styleomega2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

//import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.activities.ImageAdaptor;
import com.example.galhe.styleomega2.activities.splash;
import com.example.galhe.styleomega2.models.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by galhe on 5/1/2018.
 */

public class f_home extends android.support.v4.app.Fragment {



  //  bookedItem = cart.findWithQuery(cart.class, "Select * from cart where status = ?", "Pending");


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.home_fragment, container, false);
        final EditText Search =(EditText)view.findViewById(R.id.edittxtsearch);


        android.support.v4.app.Fragment fragment = new f_home();
        Bundle bundle = getArguments();
        String id = bundle.getString("ID");
       final List<Product> plist = Product.findWithQuery(Product.class, "Select * from Product where Category = ?", id);

       Button buttonSearch = (Button)view.findViewById(R.id.btnsearch);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searcht= Search.getText().toString();
                List<Product> plist = Product.findWithQuery(Product.class,"SELECT * FROM Product WHERE NAME like '%"+searcht+"%'");
                ImageAdaptor MainAdapter = new ImageAdaptor(getActivity(), plist);
                gridView = view.findViewById(R.id.gridview);
                gridView.setAdapter(MainAdapter);
            }
        });
        //inflate the fragment layout
        //get id of gridview
        //get products from table
//        final List<Product> productList = Product.listAll(Product.class);

        ImageAdaptor MainAdapter = new ImageAdaptor(getActivity(), plist);
        gridView = view.findViewById(R.id.gridview);
        gridView.setAdapter(MainAdapter);



        GridView Grid = (GridView) view.findViewById(R.id.gridview);
        Grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.fragment_container , new Detail_fragment());
//            ft.commit();



            Product product = plist.get(position);

            String name = product.getName();
            long productId = product.getId();
            double price = product.getPrice();
            String image = product.getFullImage();
            String description = product.getLongDescription();
            double quantity = product.getQuantity();


           Bundle bd1= new Bundle() ;
           bd1.putLong("id",productId);
           bd1.putString("Name",name);
            bd1.putDouble("Price",price);
            bd1.putString("Image",image);
            bd1.putString("Description",description);
            bd1.putDouble("Quantity",quantity);

            Detail_fragment f = new Detail_fragment();

           f.setArguments(bd1);

            android.support.v4.app.FragmentTransaction x = getActivity().getSupportFragmentManager().beginTransaction();
            x.replace(R.id.fragment_container, f);
            x.addToBackStack(null);
            x.commit();






        }
    });
        return view;

    }


    GridView gridView;



}
