package com.example.galhe.styleomega2.models;

import com.orm.SugarRecord;

/**
 * Created by galhe on 4/24/2018.
 */

public class User extends SugarRecord {

    String email;
    String password;
    String FirstName;
    String address;

    /*String lastName;
    String mobile;
    String address;
    String NIC;*/

    public User() {
    }

    public User(String email, String password, String firstName,String address) {
        this.email = email;
        this.password = password;
        FirstName = firstName;
        this.address = address;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
