package com.example.galhe.styleomega2.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.galhe.styleomega2.R;
import com.example.galhe.styleomega2.models.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

public class splash extends AppCompatActivity {
    String Json = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.splash);
        super.onCreate(savedInstanceState);

        try {
            readJsonFromAsset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Product> plist = Product.listAll(Product.class);

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(splash.this, login.class));
                finish();
            }
        }, secondsDelayed * 1000);



    }

       // startActivity(new Intent(splash.this, login.class));
        // close splash activity
        //finish();

    public String readJsonFromAsset ()throws IOException{
            InputStream is = this.getAssets().open("Product.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            Json = new String(buffer, "UTF-8");
           
            if (Product.listAll(Product.class).size() < 1) {
                Gson gson = new Gson();
                Type listType = new TypeToken<List<Product>>() {
                }.getType();
                List<Product> list = null;
                list = gson.fromJson(Json, listType);
               
                Product.saveInTx(list);
            }
          

     return Json;
      }

    }
