package com.example.galhe.styleomega2.models;

import com.orm.SugarRecord;

/**
 * Created by galhe on 5/31/2018.
 */

public class OrderedProducts extends SugarRecord  {
    cart c;
    User u;


    public OrderedProducts(cart c, User u) {
        this.c = c;
        this.u = u;

    }

    public OrderedProducts() {

    }



    public cart getC() {
        return c;
    }

    public void setC(cart c) {
        this.c = c;
    }

    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }
}
